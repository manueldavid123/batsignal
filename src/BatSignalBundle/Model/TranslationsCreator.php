<?php

namespace BatSignalBundle\Model;


use BatSignalBundle\Manager\WebData;

class TranslationsCreator
{
    private $companyType;
    private $companyName;
    private $brand;
    private $city;
    private $locales;
    private $webSlug;
    private $kernelRootDir;

    /**
     * TranslationsCreator constructor.
     * @param $companyType
     * @param $companyName
     * @param $brand
     * @param $city
     */
    public function __construct(
        $companyType,
        $companyName,
        $brand,
        $city,
        $locales,
        $webSlug,
        $kernelRootDir)
    {
        $this->companyType = $companyType;
        $this->companyName = $companyName;
        $this->brand = $brand;
        $this->city = $city;
        $this->locales = $locales;
        $this->webSlug = $webSlug;
        $this->kernelRootDir = $kernelRootDir;
    }


    public function generateTranslationsFiles()
    {
        foreach ($this->locales as $locale)
        {
            $translation = $this->getTranslationByLocaleAndCompanyName($locale);

            $customTranslation = str_replace('%companyName%', $this->companyName, $translation);
            $customTranslation = str_replace('%brand%', $this->brand, $customTranslation);
            $customTranslation = str_replace('%city%', $this->city, $customTranslation);

            $path = $this->kernelRootDir. '/../src/BatSignalBundle/Resources/export/translation/' . $this->webSlug .'/'. $locale . '.php';
            $writter = new WebDataYamlWriter($customTranslation);
            $writter->proceedToWrite($path);

        }
    }

    private function getTranslationByLocaleAndCompanyName($locale)
    {
        $translation = null;

        switch ($this->companyType)
        {
            case WebData::DEALER:
                $translation = file_get_contents($this->kernelRootDir.'/Resources/translations/dealer/' . $locale. '.php');
                break;

            case WebData::SERVICE:
                $translation = file_get_contents($this->kernelRootDir.'/Resources/translations/aftersale/' . $locale. '.php');
                break;

            case WebData::AFTERSALE:
                $translation = file_get_contents($this->kernelRootDir.'/Resources/translations/service/' . $locale. '.php');
                break;

        }

        return $translation;
    }


}