<?php

namespace BatSignalBundle\Model;

use Symfony\Component\Filesystem\Filesystem;

class WebDataYamlWriter
{
    private $data;

    /** @var Filesystem */
    private $fs;

    /**
     * WebConfigYamlWriter constructor.
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
        $this->fs = new Filesystem();
    }

    public function proceedToWrite($path)
    {
        if(NULL !== $this->data)
        {
            $this->fs->dumpFile($path, $this->data);
        }
    }

}