<?php

namespace BatSignalBundle\Model;

use BatSignalBundle\Manager\WebData;
use Symfony\Component\Yaml\Yaml;

class YamlToArrayReader
{
    public static function parse($divisionId, $kernelRootDir, $brand){
        $path = null;

        switch ($divisionId)
        {
            case WebData::DEALER:
                $path = $kernelRootDir.'/Resources/menus/'. $brand .'/dealer_menus.yml';
                break;

            case WebData::SERVICE:
                $path = $kernelRootDir.'/Resources/menus/'. $brand .'/service_menus.yml';
                break;

            case WebData::AFTERSALE:
                $path = $kernelRootDir.'/Resources/menus/'. $brand .'/aftersale_menus.yml';
                break;
        }

        return Yaml::parse(file_get_contents($path));
    }

}