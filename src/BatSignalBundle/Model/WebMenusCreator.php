<?php

namespace BatSignalBundle\Model;


class WebMenusCreator
{
    private $websections;
    private $defaultMenu;

    /**
     * WebMenusCreator constructor.
     * @param $websections
     * @param $defaultMenu
     */
    public function __construct($websections, $defaultMenu)
    {
        $this->websections = $websections;
        $this->defaultMenu = $defaultMenu;
    }

    public function giveItToMe()
    {
        $menu = array();

        foreach($this->defaultMenu as $key => $value) {
            $submenu = $this->checkAllMenus($value);
            if(count($submenu) > 0 ) {
                $menu[$key] = $submenu;
            }
        }

        return $menu;
    }

    private function checkAllMenus($data) {
        $menu = array();
        foreach ($data as $key => $value) {
            if(array_key_exists('children', $value)) {
                $submenus = $this->checkMenuChildren($value['children']);

                if(count($submenus) > 0)
                    $menu[$key]['children'] = $submenus;
            } else {
                if(isset($value['section']) and !in_array($value['section'],$this->websections))
                    $menu[$key] = NULL;
            }
        }
        return $menu;
    }

    private function checkMenuChildren($menuChildren)
    {
        $subMenu = array();
        foreach ($menuChildren as $key => $value)
        {
            if(isset($value['section']) and !in_array($value['section'], $this->websections))
                $subMenu[$key] = NULL;

        }

        return $subMenu;
    }

}