<?php

namespace BatSignalBundle\Model;

use Symfony\Component\Yaml\Dumper;

/**
 * Created by PhpStorm.
 * User: Manuel
 * Date: 21/4/16
 * Time: 23:15
 */

class WebDataToYamlConverter
{
    private $webData;

    /**
     * WebConfigToYamlInterpreter constructor.
     */
    public function __construct($webData)
    {
        $this->webData = $webData;
    }

    public function convertWebDataToYaml() {
        $dumper = new Dumper();
        if(count($this->webData) > 0 )
            return $dumper->dump($this->webData, 4);
        else
            return null;
    }

}