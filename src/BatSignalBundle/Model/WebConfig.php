<?php

namespace BatSignalBundle\Model;

/**
 * Created by PhpStorm.
 * User: Manuel
 * Date: 21/4/16
 * Time: 23:07
 */
class WebConfig
{
    private $webId;
    private $webSlug;
    private $brand;
    private $defaultLocale;
    private $allowLocales;
    private $companyName;
    private $companyShortDescription;
    private $companyFullDescription;
    private $headerPhones;
    private $footerLocation;

    /**
     * @return mixed
     */
    public function getWebId()
    {
        return $this->webId;
    }

    /**
     * @param mixed $webId
     * @return WebConfig
     */
    public function setWebId($webId)
    {
        $this->webId = $webId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getWebSlug()
    {
        return $this->webSlug;
    }

    /**
     * @param mixed $webSlug
     * @return WebConfig
     */
    public function setWebSlug($webSlug)
    {
        $this->webSlug = $webSlug;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDefaultLocale()
    {
        return $this->defaultLocale;
    }

    /**
     * @param mixed $defaultLocale
     * @return WebConfig
     */
    public function setDefaultLocale($defaultLocale)
    {
        $this->defaultLocale = $defaultLocale;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAllowLocales()
    {
        return $this->allowLocales;
    }

    /**
     * @param array $allowLocales
     * @return WebConfig
     */
    public function setAllowLocales($allowLocales)
    {
        $this->allowLocales = $allowLocales;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     * @return WebConfig
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getHeaderPhones()
    {
        return $this->headerPhones;
    }

    /**
     * @param mixed $headerPhones
     * @return WebConfig
     */
    public function setHeaderPhones($headerPhones)
    {
        $this->headerPhones = $headerPhones;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFooterLocation()
    {
        return $this->footerLocation;
    }

    /**
     * @param mixed $footerLocation
     * @return WebConfig
     */
    public function setFooterLocation($footerLocation)
    {
        $this->footerLocation = $footerLocation;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     * @return WebConfig
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyShortDescription()
    {
        return $this->companyShortDescription;
    }

    /**
     * @param mixed $companyShortDescription
     * @return WebConfig
     */
    public function setCompanyShortDescription($companyShortDescription)
    {
        $this->companyShortDescription = $companyShortDescription;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompanyFullDescription()
    {
        return $this->companyFullDescription;
    }

    /**
     * @param mixed $companyFullDescription
     * @return WebConfig
     */
    public function setCompanyFullDescription($companyFullDescription)
    {
        $this->companyFullDescription = $companyFullDescription;

        return $this;
    }

    public function toArray(){
        return array(
            "webId" => $this->webId,
            "web_slug" => $this->webSlug,
            "brand" => $this->brand,
            "default_locale" => $this->defaultLocale,
            "allow_locales" => $this->allowLocales,
            "company_name" => $this->companyName,
            "company_short_description" => $this->companyShortDescription,
            "company_full_description" => $this->companyFullDescription,
            "header_phone" => $this->headerPhones,
            "footer_location" => $this->footerLocation
        );
    }
}