<?php

namespace BatSignalBundle\Command;


use BatSignalBundle\Manager\WayneIndustries;
use BatSignalBundle\Manager\WebData;
use BatSignalBundle\Manager\WebMenus;
use BatSignalBundle\Model\BatSignal;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BatSignalCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('dapda:batsignal')
            ->setDescription('Generate all webs from the specified brand')
            ->addOption(
                'all',
                null,
                InputOption::VALUE_OPTIONAL,
                'Created all webs from the brand given'
            )
            ->addArgument(
                'webs',
                InputArgument::IS_ARRAY,
                'Creates a web (separate it to generate multiple webs)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $output->writeln(BatSignal::show());
        sleep(1);

        /** @var WebData $webData */
        $webData = $container->get('app.manager.web_data');
        /** @var WebMenus $webMenus */
        $webMenus = $container->get('app.manager.web_menus');
        $kernelRootDir = $container->get('kernel')->getRootDir();

        /** @var WayneIndustries $wayneIndustries */
        $wayneIndustries = new WayneIndustries($container, $output, $kernelRootDir, $webData, $webMenus);


        if($input->getOption('all')) {
            $brand = $input->getOption('all');
            $wayneIndustries->doAllWebsCommand($brand);
            $output->writeln("");
            $output->writeln('<question>Done</question>');
        }else {
            if($input->getArgument('webs'))
            {
                $webs = $input->getArgument('webs');
                $wayneIndustries->doSomeWebsCommand($webs);
                $output->writeln("");
                $output->writeln('<question>Done</question>');
            }
        }
    }

}