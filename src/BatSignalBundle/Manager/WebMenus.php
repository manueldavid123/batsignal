<?php

namespace BatSignalBundle\Manager;


use Doctrine\ORM\EntityManager;
use PDO;

class WebMenus
{
    /** @var  EntityManager */
    private $em;

    /**
     * WebMenus constructor.
     * @param $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getWebSections($webId)
    {
        $sql = $this->em
            ->getConnection()
            ->prepare("select slug from WebSection ws, Section s where ws.web_id = $webId and ws.section_id = s.id");

        $sql->execute();

        return $sql->fetchAll(PDO::FETCH_COLUMN);
    }



}