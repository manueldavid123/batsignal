<?php

namespace BatSignalBundle\Manager;


use BatSignalBundle\Model\WebDataYamlWriter;
use Symfony\Component\Filesystem\Filesystem;

class ResourcesCreator
{
    /** @var  Filesystem */
    private $fs;

    /** @var  WebDataYamlWriter */
    private $webDataYamlWriter;

    /**
     * ResourcesCreator constructor.
     * @param WebDataYamlWriter $webDataYamlWriter
     */
    public function __construct(WebDataYamlWriter $webDataYamlWriter)
    {
        $this->fs = new Filesystem();
        $this->webDataYamlWriter = $webDataYamlWriter;
    }

    public function proceedToMakeMagic($webSlug, $webDataPath)
    {
        $customPath = $webDataPath . '/' .$webSlug;

        if(!$this->fs->exists($customPath))
            $this->fs->mkdir($customPath);

        $this->webDataYamlWriter->proceedToWrite($customPath);
    }


}