<?php

namespace BatSignalBundle\Manager;

use Doctrine\ORM\EntityManager;
use PDO;

class WebData
{
    const DEALER = '1';
    const SERVICE = '2';
    const AFTERSALE = '3';

    /** @var  EntityManager */
    private $em;

    /**
     * WebData constructor.
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getWebConfigByBrand($brand)
    {
        $sql = $this->em->getConnection()->prepare("select * from WebConfig where brandImage_id = (select id from Brand where slug = '$brand')");
        $sql->execute();

        return $this->getWebDataFromWebConfig($sql->fetchAll(PDO::FETCH_COLUMN));
    }

    public function getWebConfigByWebSlug($webSlug)
    {
        $sql = $this->em->getConnection()->prepare("select w.*, b.`slug` 'brand' from Web w, Brand b, WebConfig wc where w.slug='".$webSlug."' and wc.web_id = w.id and wc.brandImage_id = b.`id`");
        $sql->execute();

        return $sql->fetchAll(PDO::FETCH_ASSOC)[0];
    }

    private function getWebDataFromWebConfig($webIds) {

        $dataArray = array();

        foreach ($webIds as $webId)
        {
            $sql = $this->em->getConnection()->prepare("select * from Web where id = $webId");

            $sql->execute();

            $dataArray[] = $sql->fetch(PDO::FETCH_ASSOC);
        }

        return $dataArray;

    }

    public function getLanguageNameByLanguageId($id)
    {
        $sql = $this->em->getConnection()->prepare("select locale from Language where id = $id");

        $sql->execute();

        return $sql->fetchColumn(0);
    }

    public function getAllowLanguagesByWebId($webId) {
        $sql = $this->em->getConnection()->prepare("select language_id from web_language where web_id = $webId");

        $sql->execute();

        $allowLocales = $sql->fetchAll();
        $allowLocalesArray = array();

        foreach ($allowLocales as $locale)
        {
            array_push($allowLocalesArray, $this->getLanguageNameByLanguageId($locale['language_id']));
        }

        return $allowLocalesArray;
    }

    public function getInstallationByWebId($webId) {
        $sql = $this->em->getConnection()->prepare("select id, town_id, province_id, address, postalCode from Installation where web_id = $webId ORDER BY sort ASC LIMIT 1");
        $sql->execute();
        $installationData = $sql->fetchAll();

        $phonesAndEmails = $this->getPhonesAndEmailsByInstallationId($installationData[0]['id']);

        $city = $this->getFullCityAddress($installationData[0]['town_id'], $installationData[0]['province_id'], $installationData[0]['postalCode']);


        $contacto['address'] = $installationData[0]['address'];
        $contacto['city'] = $city;

        if (isset($phonesAndEmails['phones']) and count($phonesAndEmails['phones']) > 0)
            $contacto['phones'] = $phonesAndEmails['phones'];

        if (isset($phonesAndEmails['faxes']) and count($phonesAndEmails['faxes']) > 0)
            $contacto['faxes'] = $phonesAndEmails['faxes'];


        $contacto['emails'] = $phonesAndEmails['emails'];

        return $contacto;
    }

    public function getInstallationTownAndCityByWebId($webId) {
        $sql = $this->em->getConnection()->prepare("select id, town_id, province_id from Installation where web_id = $webId ORDER BY sort ASC LIMIT 1");
        $sql->execute();
        $installationData = $sql->fetchAll();

        $city = $this->getTownAndProvinces($installationData[0]['town_id'], $installationData[0]['province_id']);

        return $city;
    }

    private function getPhonesAndEmailsByInstallationId($installationId) {
        $sql = $this->em->getConnection()->prepare("select number from InstallationPhone where installation_id = $installationId and isFax = 0 and isWhatsapp = 0 ORDER BY sort ASC");
        $sql->execute();

        $phones = $sql->fetchAll();

        $sql = $this->em->getConnection()->prepare("select number from InstallationPhone where installation_id = $installationId and isFax = 1 and isWhatsapp = 0 ORDER BY sort ASC");
        $sql->execute();

        $faxes = $sql->fetchAll();

        $sql = $this->em->getConnection()->prepare("select number from InstallationPhone where installation_id = $installationId and isFax = 0 and isWhatsapp = 1 ORDER BY sort ASC");
        $sql->execute();

        $whatsapps = $sql->fetchAll();

        $sql = $this->em->getConnection()->prepare("select email from InstallationEmail where installation_id = $installationId");
        $sql->execute();

        $emails = $sql->fetchAll(PDO::FETCH_COLUMN);

        $datosTel = [];

        foreach ($phones as $phone) {
            $array = [
                'number' => $phone['number'],
                'link' => "+34".$phone['number']
            ];

            array_push($datosTel,$array);
        }

        foreach ($whatsapps as $phone) {
            $array = [
                'number' => $phone['number'],
                'link' => "+34".$phone['number']
            ];

            array_push($datosTel,$array);
        }

        $datosFax = [];

        foreach ($faxes as $fax) {
            $array = [
                'number' => $fax['number']
            ];

            array_push($datosFax,$array);
        }

        $datosFinales = [];

        if (count($datosTel) > 0) {
            $datosFinales['phones'] = $datosTel;
        }

        if (count($datosFax) > 0) {
            $datosFinales['faxes'] = $datosFax;
        }

        $datosFinales['emails'] = $emails;

        return $datosFinales;

    }

    private function getTownAndProvinces($town_id, $province_id)
    {
        $sql = $this->em->getConnection()->prepare("select name from Province where id = $province_id");
        $sql->execute();

        $province = $sql->fetchColumn(0);

        $sql = $this->em->getConnection()->prepare("select name from Town where id = $town_id");
        $sql->execute();

        $town = $sql->fetchColumn(0);
        if($town === $province)
            $fullCityAndProvinceName = $town;
        else
            $fullCityAndProvinceName = $town ." (" . $province.")";

        return $fullCityAndProvinceName;
    }

    private function getFullCityAddress($town_id, $province_id, $postalCode)
    {
        $sql = $this->em->getConnection()->prepare("select name from Province where id = $province_id");
        $sql->execute();

        $province = $sql->fetchColumn(0);

        $sql = $this->em->getConnection()->prepare("select name from Town where id = $town_id");
        $sql->execute();

        $town = $sql->fetchColumn(0);

        $fullCityAndProvinceName = $postalCode . " " . $town . " (" . $province. ")";

        return $fullCityAndProvinceName;
    }



    public function getHeaderPhoneByWebId($webId)
    {
        $sql = $this->em->getConnection()->prepare("select id from Installation where web_id = $webId ORDER BY sort ASC LIMIT 1");
        $sql->execute();
        $installationId = $sql->fetchColumn(0);

        $sql = $this->em->getConnection()->prepare("select number from InstallationPhone where installation_id = $installationId and isFax = 0 and isWhatsapp = 0 ORDER BY sort ASC LIMIT 1");
        $sql->execute();

        $phone = $sql->fetchColumn(0);

        $array['phone'] = $phone;
        $array['link'] = '+34'.$phone;

        return $array;
    }

    public function getBrandByBrandSlug($brandSlug)
    {
        $sql = $this->em->getConnection()->prepare(" select name from Brand where slug = '$brandSlug' ");
        $sql->execute();

        return $sql->fetchColumn(0);
    }



}