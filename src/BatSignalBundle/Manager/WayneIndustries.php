<?php
/**
 * Created by PhpStorm.
 * User: dapda
 * Date: 28/04/16
 * Time: 9:12
 */

namespace BatSignalBundle\Manager;


use BatSignalBundle\Model\TranslationsCreator;
use BatSignalBundle\Model\WebConfig;
use BatSignalBundle\Model\WebDataToYamlConverter;
use BatSignalBundle\Model\WebDataYamlWriter;
use BatSignalBundle\Model\WebMenusCreator;
use BatSignalBundle\Model\YamlToArrayReader;
use Symfony\Component\Console\Helper\ProgressBar;

class WayneIndustries
{
    private $container;
    private $output;
    private $kernelRootDir;
    /** @var  WebData  */
    private $webData;
    /** @var  WebMenus */
    private $webMenus;

    public function __construct($container, $output, $kernelRootDir, WebData $webData, WebMenus $webMenus)
    {
        $this->container = $container;
        $this->output = $output;
        $this->kernelRootDir = $kernelRootDir;
        $this->webData = $webData;
        $this->webMenus = $webMenus;
    }


    public function doAllWebsCommand($brand)
    {

        $allData = $this->webData->getWebConfigByBrand($brand);

        $progress = $this->generateProgressBar(count($allData));

        foreach($allData as $datos) {

            $this->generateWebFiles($brand, $datos);

            $progress->advance();
        }

        $progress->finish();

    }

    public function doSomeWebsCommand($webSlugs)
    {

        $progress = $this->generateProgressBar(count($webSlugs));

        foreach($webSlugs as $webSlug) {

            $datos = $this->webData->getWebConfigByWebSlug($webSlug);

            $this->generateWebFiles($datos['brand'],$datos);

            $progress->advance();
        }

        $progress->finish();

    }


    private function generateProgressBar($websNumber) {
        $progress = new ProgressBar($this->output, $websNumber);
        $progress->setBarCharacter('<bg=green>=</>');
        $progress->setProgressCharacter(">");

        return $progress;
    }


    protected function printData($brand, $datos, WebData $webData)
    {
        $webConfig = new WebConfig();
        $webConfig->setBrand($brand);
        $webConfig->setWebSlug($datos['slug']);
        $webConfig->setWebId($datos['id']);
        $webConfig->setAllowLocales($webData->getAllowLanguagesByWebId($datos['id']));
        $webConfig->setCompanyName($datos['companyName']);
        $webConfig->setCompanyShortDescription("company.short_description");
        $webConfig->setCompanyFullDescription("company.full_description");
        $webConfig->setDefaultLocale($webData->getLanguageNameByLanguageId($datos['defaultLanguage_id']));
        $webConfig->setFooterLocation($webData->getInstallationByWebId($datos['id']));
        $webConfig->setHeaderPhones($webData->getHeaderPhoneByWebId($datos['id']));
        return $webConfig;
    }

    protected function generateWebFiles($brand, $datos)
    {
        $webConfig = $this->printData($brand, $datos, $this->webData);
        $webConfigToYaml = new WebDataToYamlConverter($webConfig->toArray());

        $yamlWebConfig = $webConfigToYaml->convertWebDataToYaml();

        $webSections = $this->webMenus->getWebSections($datos['id']);
        $webMenusCreator = new WebMenusCreator(
            $webSections,
            YamlToArrayReader::parse($datos['division_id'], $this->kernelRootDir, $webConfig->getBrand())
        );

        $menus = $webMenusCreator->giveItToMe();
        $webMenusToYaml = new WebDataToYamlConverter($menus);

        $yamlWebMenus = $webMenusToYaml->convertWebDataToYaml();

        $translationCreator = new TranslationsCreator(
            $datos['division_id'],
            $webConfig->getCompanyName(),
            $this->webData->getBrandByBrandSlug($webConfig->getBrand()),
            $this->webData->getInstallationTownAndCityByWebId($datos['id']),
            $webConfig->getAllowLocales(),
            $webConfig->getWebSlug(),
            $this->container->get('kernel')->getRootDir()
        );

        $translationCreator->generateTranslationsFiles();
        $webConfigWritter = new WebDataYamlWriter($yamlWebConfig);
        $webConfigWritter->proceedToWrite($this->kernelRootDir . '/../src/BatSignalBundle/Resources/export/config/' . $webConfig->getWebSlug() . '/config.yml');
        $webMenusWritter = new WebDataYamlWriter($yamlWebMenus);
        $webMenusWritter->proceedToWrite($this->kernelRootDir . '/../src/BatSignalBundle/Resources/export/config/' . $webConfig->getWebSlug() . '/menus.yml');
    }
}