<?php

return array(
    "company.short_description" => "Agent Postvenda %brand% a %city",
    "company.full_description" => "%companyName%, Agent Postvenda %brand% a %city%",
);