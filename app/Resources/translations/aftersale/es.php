<?php

return array(
    "company.short_description" => "Agente Posventa %brand% en %city%",
    "company.full_description" => "%companyName%, Agente Posventa %brand% en %city%",
);