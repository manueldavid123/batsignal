<?php

return array(
    "company.short_description" => "%brand% Salmenta Agente %city%",
    "company.full_description" => "%companyName%, %brand% Salmenta Agente %city%"
);