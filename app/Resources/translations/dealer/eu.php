<?php

return array(
    "company.short_description" => "%brand% Lizentziadun Ofiziala %city%",
    "company.full_description" => "%companyName%, %brand% Lizentziadun Ofiziala %city%"
);