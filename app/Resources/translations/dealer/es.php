<?php

return array(
    "company.short_description" => "Concesionario Oficial %brand% en %city%",
    "company.full_description" => "%companyName%, Concesionario Oficial %brand% en %city%",
);