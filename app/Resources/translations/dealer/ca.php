<?php

return array(
    "company.short_description" => "Concessionari Oficial %brand% a %city%",
    "company.full_description" => "%companyName%, Concessionari Oficial %brand% a %city%",
);