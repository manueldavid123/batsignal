<?php

return array(
    "company.short_description" => "Servei Oficial %brand% a %city%",
    "company.full_description" => "%companyName%, Servei Oficial %brand% a %city%",
);