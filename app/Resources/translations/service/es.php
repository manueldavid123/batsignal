<?php

return array(
    "company.short_description" => "Servicio Oficial %brand% en %city%",
    "company.full_description" => "%companyName%, Servicio Oficial %brand% en %city%",
);